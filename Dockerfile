FROM debian:buster-slim
RUN apt-get update && \
    apt-get dist-upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install apt-utils -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install zip jq curl -y && \
    DEBIAN_FRONTEND=noninteractive apt-get purge apt-utils -y && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip  && \
    ./aws/install && \
    apt-get install -y wget apt-transport-https gnupg groff && \
    wget -O public "https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public" && \
    gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --import public && \
    gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --export --output adoptopenjdk-archive-keyring.gpg && \
    rm public  adoptopenjdk-keyring.gpg && \
    mv adoptopenjdk-archive-keyring.gpg /usr/share/keyrings && \
    echo "deb [signed-by=/usr/share/keyrings/adoptopenjdk-archive-keyring.gpg] https://adoptopenjdk.jfrog.io/adoptopenjdk/deb buster main" | tee /etc/apt/sources.list.d/adoptopenjdk.list && \
    apt-get update && \
    mkdir -p /usr/share/man/man1 && \
    apt-get install adoptopenjdk-16-hotspot -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*  && \
    useradd  -ms /bin/bash javauser

USER javauser
WORKDIR /home/javauser
RUN wget -O flyway-commandline-7.12.1-linux-x64.tar.gz "https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/7.12.1/flyway-commandline-7.12.1-linux-x64.tar.gz" && \
    tar xzf flyway-commandline-7.12.1-linux-x64.tar.gz
ENV PATH=/home/javauser/flyway-7.12.1:$PATH

